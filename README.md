# Code Signing Demo

This is a .Net 6 project demoing code signing using Ancora's [`Ancora.CodeSignTool.MSBuild`](https://www.nuget.org/packages/Ancora.CodeSignTool.MSBuild/) build task.
This demo project requires the following installed on the system:

* [Java 11 or newer](https://www.oracle.com/java/technologies/downloads/#java11)
* [.Net 6 SDK](https://dotnet.microsoft.com/en-us/download/dotnet/6.0) (although the build task will work for any version of Visul Studio/MSBuild and .Net)

# Building

1. Edit [`Test.csproj`](.\Test.csproj) and supply valid values for the following properties of the `Ancora.MSBuild.CodeSignTool` build task (lines 21 - 24):

* `Username`
* `Password`
* `CredentialId`
* `TOTPSecret`

2. Build the project from the command line by invoked the folloiwng PowerShell command from within the repository root directory:

```PowerShell
dotnet build
```

The output file `bin\Debug\net6.0\Test.exe` should be digitally signed.
